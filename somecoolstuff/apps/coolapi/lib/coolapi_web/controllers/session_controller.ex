defmodule CoolapiWeb.SessionController do
  use CoolapiWeb, :controller
  use OAuth2.Strategy

  # alias Coolapi.Auth
  # alias Coolapi.Auth.Session
  alias Coolapi.Accounts
  alias Coolapi.Accounts.User

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def login(conn, params) do
    IO.inspect(params)

    case Accounts.get_user_by_email(params["email"]) do
      require IEx
      IEx.pry()
      {:ok, user} ->
        IO.inspect(user)

      {:error, info} ->
        IO.inspect(info)
    end

    render(conn, "new.html")
  end

  defp create_client() do
    OAuth2.Client.new(
      strategy: __MODULE__,
      client_id: 1
    )
  end
end
