defmodule CoolapiWeb.Router do
  use CoolapiWeb, :router

  alias CampusLabWeb.Plug.AuthenticateToken

  pipeline :browser do
    plug(:accepts, ["json","html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :protected do
    plug(:accepts, ["json","html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)

    plug(AuthenticateToken)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", CoolapiWeb do
    pipe_through(:protected)



  end

  scope "/", CoolapiWeb do
    pipe_through(:browser)

    get("/", PageController, :index)
    resources("/users", UserController, only: [:new, :create])
    get("/auth/new", SessionController, :new)
    post("/auth", SessionController, :login)
  end

  # Other scopes may use custom stacks.
  # scope "/api", CoolapiWeb do
  #   pipe_through :api
  # end
end
