defmodule Coolapi.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  @create_fields ~w(name password email)a
  @optional_fields ~w(token)a

  schema "users" do
    field(:email, :string)
    field(:name, :string)
    field(:password, :string)
    field(:token, :string)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    attrs = Map.put(attrs, "token", Ecto.UUID.generate())
    user
    |> cast(attrs, @create_fields ++ @optional_fields)
    |> validate_required(@create_fields)
  end
end
