# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :coolapi,
  ecto_repos: [Coolapi.Repo]

# Configures the endpoint
config :coolapi, CoolapiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ZKS482mWbgsNNDxcLoU6vUGK3Qv0dWNXo3mBIop3j792YrnU0/AsZBm6NNO6f2he",
  render_errors: [view: CoolapiWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Coolapi.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :oauth2,
  serializers: %{
    "application/vnd.api+json" => Poison,
    "application/xml" => MyApp.XmlParser
  }

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
