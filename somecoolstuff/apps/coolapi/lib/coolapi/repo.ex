defmodule Coolapi.Repo do
  use Ecto.Repo,
    otp_app: :coolapi,
    adapter: Ecto.Adapters.Postgres
end
